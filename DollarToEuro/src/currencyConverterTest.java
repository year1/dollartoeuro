import junit.framework.TestCase;

public class currencyConverterTest extends TestCase 
{
	/*	Test 001
	 * 	Objective:	Test for minimal boundary values for Dollar and Exchange
	 * 	Input Dollar = minDouble
	 * 	Input Exchange = minDouble
	 * 	Expected output = -1
	 */
	public void testcurrencyConverterDollarTOeuro001()
	{
		assertEquals(-1,currencyConverter.dollarTOeuro(-Double.MAX_VALUE, -Double.MAX_VALUE),0.005);
	}

	/*	Test 002
	 * 	Objective:	Test for boundary value of maximum lower rejected dollar and maximum lower rejected exchange
	 * 	Input Dollar = 99.99
	 * 	Input Exchange = 0.049
	 * 	Expected output = -1
	 */
	public void testcurrencyConverterDollarTOeuro002()
	{
		assertEquals(-1,currencyConverter.dollarTOeuro(99.99, 0.049),0.005);
	}
	
	/*	Test 003
	 * 	Objective:	Test for boundary value of minimal accepted dollar and minimal accepted exchange rate
	 * 	Input Dollar = 100
	 * 	Input Exchange = 0.5
	 * 	Expected output = 50
	 */
	public void testcurrencyConverterDollarTOeuro003()
	{
		assertEquals(50,currencyConverter.dollarTOeuro(100, 0.5),0.005);
	}

	/*	Test 004
	 * 	Objective:	Test for boundary value of maximum accepted dollar and maximum accepted exchange rate
	 * 	Input Dollar = 10'000
	 * 	Input Exchange = 1.5
	 * 	Expected output = 15000
	 */
	public void testcurrencyConverterDollarTOeuro004()
	{
		assertEquals(15000,currencyConverter.dollarTOeuro(10000, 1.5),0.005);
	}

	/*	Test 005
	 * 	Objective:	Test for boundary value of higher minimum rejected Dollar and higher minimum rejected Exchange rate
	 * 	Input Dollar = 10'000.001
	 * 	Input Exchange = 1.501
	 * 	Expected output = -1
	 */
	public void testcurrencyConverterDollarTOeuro005()
	{
		assertEquals(-1,currencyConverter.dollarTOeuro(10000.001, 1.501),0.005);
	}

	/*	Test 006
	 * 	Objective:	Test for boundary value of higher maximum rejected Dollar and higher maximum rejected exchange
	 * 	Input Dollar = maxDouble
	 * 	Input Exchange = maxDouble
	 * 	Expected output = -1
	 */
	public void testcurrencyConverterDollarTOeuro006()
	{
		assertEquals(-1,currencyConverter.dollarTOeuro(Double.MAX_VALUE, Double.MAX_VALUE),0.005);
	}
}
