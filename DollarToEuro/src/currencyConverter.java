
public class currencyConverter 
{
	public static double dollarTOeuro(double dollar, double exchange)
	{
		if(dollar <100 || dollar > 10000 || exchange < 0.5 || exchange >1.5)
		{
			return -1;
		}
		else
		{
			return dollar*exchange;
		}
	}

}
